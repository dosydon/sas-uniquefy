#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import argparse
from sas import SAS3

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("sas_file")
    args = parser.parse_args()

    sas = SAS3.from_file(args.sas_file)

    for i,operator in enumerate(sas.operators):
        operator.name = operator.name.strip() + str(i)
    print(str(sas))
